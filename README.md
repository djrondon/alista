# ALISTA

Supermarket's DropShipping 

APP that integrates prices from **supermarkets** located in Brasilia, Federal District, Brazil.

## **Pitch** with Screenshots [here](https://docs.google.com/presentation/d/1rCNWHKdM90_T0KJkX8Yb7mOXwtGt5pHew4rNlPugcfk/edit?usp=sharing)

## How it works

1. You can access a demo at https://alista.com.br/go.html, as SSL is not implemented because of abscence of certificated,
you can proceed to advance.
2. When the app loads, you click at the microphone and it will ask for you to grant access to your microphone.
3. Spell the name of the product and click search button, it will search for the product.
4. Add to the cart the product that you consider the cheapest one. 
5. After you add multiple products to the cart, click at the cart, you will see a list with all of your products.


## Todo list

- Integrate with payment methods to process everything as one shipping integrated with all supermakets.
- Define profits with the app administration and expenses with delivery to the user
- Define delivery logistics to the consumer.
- Geolocation e routes definition.
- Implement a recomendation system based on products included at the list.






