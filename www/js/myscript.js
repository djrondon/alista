function objSort() {
    var args = arguments,
        array = args[0],
        case_sensitive, keys_length, key, desc, a, b, i;

    if (typeof arguments[arguments.length - 1] === 'boolean') {
        case_sensitive = arguments[arguments.length - 1];
        keys_length = arguments.length - 1;
    } else {
        case_sensitive = false;
        keys_length = arguments.length;
    }

    return array.sort(function (obj1, obj2) {
        for (i = 1; i < keys_length; i++) {
            key = args[i];
            if (typeof key !== 'string') {
                desc = key[1];
                key = key[0];
                a = obj1[args[i][0]];
                b = obj2[args[i][0]];
            } else {
                desc = false;
                a = obj1[args[i]];
                b = obj2[args[i]];
            }

            if (case_sensitive === false && typeof a === 'string') {
                a = a.toLowerCase();
                b = b.toLowerCase();
            }

            if (! desc) {
                if (a < b) return -1;
                if (a > b) return 1;
            } else {
                if (a > b) return -1;
                if (a < b) return 1;
            }
        }
        return 0;
    });
} //end of objSort() function



 function real(numero)
    {
        
        texto = numero.toString(); 
        texto = texto.replace("." , ","); // substitui a vírgula por ponto 

        return texto;
    }


// create daily localstorage

var cartcount = 0;
var cartproducts = [];
var today = new Date();
var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

if (localStorage.getItem(date) === null) {
var str = JSON.stringify(cartproducts);
localStorage.setItem(date, str);
} else {
 
 cartproducts = JSON.parse(localStorage.getItem(date));  
 cartcount = cartproducts.length;
$("#cartnumber.notification").html(cartcount);  

}

 
function update() {
$("#cartnumber.notification").html(cartcount);  
document.getElementById("transcript").value = "";
}

console.log(date);
console.log(cartcount);



function addtocart(store,product, price,logo,qty) {

var options = {
                title:'Adicionado ao carrinho:',
                message: product
              }
ons.notification.alert(options);
//alert(obj);
qty = qty + '.value';
qty = eval(qty);
cartcount = cartcount + 1;
var dataproducts = {store:  store ,product:  product ,logo:  logo , price: price , qty:  qty };
cartproducts.push(dataproducts);
$("#cartnumber.notification").html(cartcount);
var str = JSON.stringify(cartproducts);
localStorage.setItem(date, str);
console.log(cartproducts);
}



function cleanCart() {

cartcount = 0;
cartproducts = [];
var str = JSON.stringify(cartproducts);
localStorage.setItem(date, str);
var options = {
                title:'Removendo lista:',
                message: 'Limpando lista de compras '
              }
ons.notification.alert(options);


}



function showLists() {

var output = '<br><br><br>';
output +='<ons-list>'
output += '<ons-list-header>Listas de compras</ons-list-header>';
for (var key in localStorage){
    output += '<ons-list-item tappable onclick="shareList();"><ons-icon icon="fa-shopping-basket">&nbsp; ' + key.trim() + ' &nbsp;<ons-icon size="large" icon="ion-ios-upload-outline"></ons-icon></ons-list-item>';
  }


$('#results').html(output).trigger('create');
$("#cartnumber.notification").html(cartcount);  
}



function showCart() {

// Sort products by store and product
objSort(cartproducts, 'store', 'product')

// display the list
var flag = 1;
var store2 = '';
var subtotal = 0.0;
var total = 0.0;
var totcomp = 0.0;

var output = '<div id="lista"><ons-card>';
    output+= 'Lista de Produtos por Supermercado';
    //output+='<ons-button modifier="quiet" "><ons-icon icon="fa-save">&nbsp;Salvar</ons-icon></ons-button>&nbsp;&nbsp;';
    //output+='<ons-button modifier="quiet" "><ons-icon icon="fa-share-alt">&nbsp; Compartilhar</ons-icon></ons-button>';
    output+='<hr>';
    output+='<ons-list>';

$.each(cartproducts,function(key,val){

     var store = val.store;
     var product = val.product;
     var price = val.price;
     var qty = val.qty;
     var logo = val.logo;





/*<ons-card>
    <img src="https://monaca.io/img/logos/download_image_onsenui_01.png" alt="Onsen UI" style="width: 100%">
    <div class="title">
      Awesome framework
    </div>
    <div class="content">
      <div>
        <ons-button><ons-icon icon="ion-thumbsup"></ons-icon></ons-button>
        <ons-button><ons-icon icon="ion-share"></ons-icon></ons-button>
      </div>
      <ons-list>
        <ons-list-header>Bindings</ons-list-header>
        <ons-list-item>Vue</ons-list-item>
        <ons-list-item>Angular</ons-list-item>
        <ons-list-item>React</ons-list-item>
      </ons-list>
    </div>
  </ons-card>
</ons-page>
*/

total = total + (parseFloat(price) * parseInt(qty));


if ((store !== store2) && (flag === 0)) {
   console.log(store2 + '->subtotal: ' + subtotal);
   output+='<ons-list-item><ons-row>';
   output+='<ons-col width="50%"><strong>Subtotal</strong></ons-col><ons-col></ons-col><ons-col></ons-col><ons-col><strong>R$ ' + real(subtotal) + '</strong></ons-col>';
   output+='</ons-row></ons-list-item>';

   flag = 1;
   totcomp = 0.0;    
} 


if (flag === 1) {
   store2 = store; 
   flag = 0;
   output+='<ons-list-header><img align="absmiddle" src="images/' + logo + '" alt="logo" style="width: 50px">&nbsp;'+ store + '</ons-list-header>';
   output+='<ons-list-item><ons-row><ons-col width="50%"><strong>Produto</strong></ons-col><ons-col><strong>Qtd</strong></ons-col><ons-col><strong>Preço Unitário</strong></ons-col><ons-col><strong>Total</strong></ons-col></ons-row></ons-list-item>';
 }

//if (store === store2) {
   output+='<ons-list-item><ons-row>';
   output+='<ons-col width="50%">' + product + '</ons-col><ons-col>' + qty + '</ons-col><ons-col>R$ ' + real(parseFloat(price)) + '</ons-col><ons-col>R$ ' + real((parseFloat(price) * parseInt(qty))) + '</ons-col>';
   output+='</ons-row></ons-list-item>';   
   totcomp = totcomp + (parseFloat(price) * parseInt(qty));
   subtotal = parseFloat(totcomp).toFixed(2); 
//} 


console.log(store);

  });

   output+='<ons-list-item><ons-row>';
   output+='<ons-col width="50%"><strong>Subtotal</strong></ons-col><ons-col></ons-col><ons-col></ons-col><ons-col><strong>R$ ' + real(subtotal) + '</strong></ons-col>';
   output+='</ons-row></ons-list-item>';



console.log(store2 + '->subtotal: ' + subtotal);
   console.log('total: ' + total);
total = parseFloat(total).toFixed(2);
   output+='<hr>'
   output+='<ons-list-item><ons-row>';
   output+='<ons-col width="50%"><strong>Total</strong></ons-col><ons-col></ons-col><ons-col></ons-col><ons-col><strong>R$ ' + real(total) + '</strong></ons-col>';
   output+='</ons-row></ons-list-item>';


console.log(cartproducts);

output+='</ons-list>';
output+='</div>';
output+='</ons-card>';
$('#results').html(output).trigger('create');
$("#cartnumber.notification").html(cartcount);  
}


function incNumber(id) {
        i = document.getElementById(id).value;
        i = parseInt(i);
        i = i + 1; 
        document.getElementById(id).value=i;
    }

    function decNumber(id) {
        i = document.getElementById(id).value;
        i = parseInt(i);
        i = i - 1; 
        if (i < 1) {
          i = 1;
        }
        document.getElementById(id).value=i;
    }






function  getPrice(){ 

          
          keyword = $("#transcript").val();
          var words = keyword.replace(new RegExp(" ", 'g'), ".*");
          console.log(words);
          
           $.ajax({
            dataType: "json",
            type: "GET",
            url: 'https://api.mlab.com/api/1/databases/market/collections/market?apiKey=PSNzCW3eYdclUXcoYReEC8W65tbXt9v3&q={%22title%22:{%22$regex%22:%22^'  + encodeURIComponent(words) + '%22, %22$options%22 : %22i%22}}&s={%22mprice%22:%201}',
            success: function (data) {
            console.log(data);


//var output = '<h2>' +  key + '</h2>';
var output ='';

output +='<div class="button-bar" align="center" style="width:95%;">';
output +='<div class="button-bar__item">';
output +='<button class="button-bar__button" selected>Produto</button>';
output +='</div>';
output +='<div class="button-bar__item">';
output +='<button class="button-bar__button">Preço</button>';
output +='</div>';
output +='<div class="button-bar__item">';
output +='<button class="button-bar__button">Supermercado</button>';
output +='</div>';
output +='</div>';
output +='<ons-list>';
output +='<ons-list-header>Resultados da busca</ons-list-header>';

$("#cartnumber.notification").html(cartcount);  

var prod = 0;

  $.each(data,function(key,val){

      var title = val.title;
      var thumbnail = val.thumbnail;
      var mprice = val.mprice;
      var store = val.store;
      var logo = val.logo;
mprice = parseFloat(mprice).toFixed(2);
var realprice = real(mprice);

prod = prod + 1;
var prodid = "p" + prod;

output +='<ons-list-item>';
   
output +='<div class="list-item__left">';
output +='<img  src="images/' + logo + '" width="40px">';
output +='</div>';

output +='<div class="list-item__left">';
output +='<img class="list-item__thumbnail" id="item" src="' + thumbnail +'" width="150px">';
output +='</div>';


output +='<div class="list-item__left">';   
output +='<div class="list__item__title">' + title + '</div><br>';
output +='<div class="list__item__title"><strong style="color:#ff9500; font-weight:800;font-size:15pt;">R$ ' + realprice + '</strong></div><br></div>';


output +='<ons-row>';
output +='<div class="list-item__right">';
output +='<ons-button modifier="cta" style="float:left;border-radius:50%;background-color: #009418;" onclick="decNumber(\'' + prodid + '\');"><ons-icon icon="fa-minus"  fixed-width="true"></ons-icon></ons-button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>';

output +='<div class="list-item__right">';
output +='<ons-input name="' + prodid + '" id="' + prodid + '" width="30px;" type="number" modifier="underline" value="1"  min="1" max="200" ></ons-input>';
output +='</div>';

output +='<div class="list-item__right">';
output +='<ons-button modifier="cta" style="border-radius:50%;background-color: #009418;" onclick="incNumber(\'' + prodid + '\');">';
output +='<ons-icon icon="fa-plus" fixed-width="true"></ons-icon></ons-button>';
output +='&nbsp;';
output +='</div>';

output +='<div class="list-item__right" >';
//output +='<ons-button modifier="cta" style="border-radius:50%;background-color: #FFCC00;" onclick="ons.notification.alert(\'Ainda vai funcionar Carcara!\');"><ons-icon icon="fa-cart-plus" fixed-width="true"></ons-icon></ons-button></div>';
output +='<ons-button modifier="cta"  style="border-radius:50%;background-color: #FFCC00;" onclick="addtocart(\'' + store + '\',\'' + title + '\',\'' + mprice + '\',\'' + logo + '\',\'' + prodid + '\');" ><ons-icon icon="fa-cart-plus" fixed-width="true" ></ons-icon></ons-button></div>';
output +='</div>';

output +-'</ons-row>';

output +='</ons-list-item>';


});


$('#results').html(output).trigger('create');

            }
        });

  



} // end function getPrice






  function startDictation() {
 
var SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent



      
      var recognition = new SpeechRecognition();
 
      recognition.continuous = false;
      recognition.interimResults = false;
 
      recognition.lang = "pt-BR";
      recognition.start();
 
      recognition.onresult = function(e) {
        document.getElementById('transcript').value
                                 = e.results[0][0].transcript;
        recognition.stop();
//        document.getElementById('labnol').submit();

      
 
       recognition.onerror = function(e) {
        recognition.stop();
      }
 
    }



  
  } // startDictation


